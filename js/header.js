(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['header.hb'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  return " current-menu-item page_item page-item-20 current_page_item";
  }

function program3(depth0,data) {
  
  
  return "current-menu-item page_item page-item-20 current_page_item";
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<div class=\"page-title-container\">\n        <div class=\"page-title\">\n            <div class=\"page-title-wrapper\">\n                <div class=\"h-left\">\n                    <div class=\"h-left-content\">\n                        <h1>"
    + escapeExpression(((stack1 = ((stack1 = depth0.subPage),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h1>\n                        <ul class=\"breadcrumbs\"><li> <a href=\"index.html\">Home</a></li><li>"
    + escapeExpression(((stack1 = ((stack1 = depth0.subPage),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</li></ul>                                                                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n";
  return buffer;
  }

  buffer += "<div class=\"avada-row\" style=\"margin-top:0px;margin-bottom:0px;\">\n            <div class=\"logo\" style=\"margin-left:0px;\">\n            <a href=\"index.html\">\n           		 <div><img src=\"images/master_logo_.jpg\" alt=\"\"></div>\n            </a>\n             <span class=\"vertical-menu-holder\">\n               <dl class=\"mobile-vertical-menu\">\n               	<dd>\n                <a href=\"index.html\">Home</a></dd><dd><a href=\"whatweoffer.html\">What We Offer</a></dd><dd><a href=\"about.html\">About Us</a></dd><dd><a href=\"testimonials.html\">Testimonials</a></dd><dd><a href=\"contact.html\">Contact Us Now</a></dd>\n                <div class=\"ac\">\n					<span class=\"teleicon\"><img src=\"/images/tele_icon.png\"/>Questions about pricing?<br/>    Contact us at: 1-888-391-3323</span>\n					</div>\n               </dl>\n               \n               		\n               \n             </span>\n            </div>\n              \n              \n            <nav id=\"nav\" class=\"nav-holder\">\n	                <ul id=\"nav\" class=\"menu\">\n					<li id=\"menu-item-48\" class=\"menu-item menu-item-type-post_type menu-item-object-page ";
  stack1 = helpers['if'].call(depth0, depth0.homeisActive, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " menu-item-48\"><a href=\"index.html\">Home</a></li>\n					<li id=\"menu-item-53\" class=\"menu-item menu-item-type-post_type menu-item-object-page ";
  stack1 = helpers['if'].call(depth0, depth0.whatisActive, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "menu-item-53\"><a href=\"whatweoffer.html\">What We Offer</a></li>\n					<li id=\"menu-item-52\" class=\"menu-item menu-item-type-post_type menu-item-object-page ";
  stack1 = helpers['if'].call(depth0, depth0.aboutisActive, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "menu-item-52\"><a href=\"about.html\">About Us</a></li>\n					<li id=\"menu-item-51\" class=\"menu-item menu-item-type-post_type menu-item-object-page ";
  stack1 = helpers['if'].call(depth0, depth0.testimonialsisActive, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "menu-item-51\"><a href=\"testimonials.html\">Testimonials</a></li>\n					<li id=\"menu-item-50\" class=\"menu-item menu-item-type-post_type menu-item-object-page ";
  stack1 = helpers['if'].call(depth0, depth0.contactisActive, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "menu-item-50\"><a href=\"contact.html\">Contact Us Now</a></li>\n					</ul>\n\n					<div class=\"ac contactgotoinfo\">\n					<span class=\"teleicon\"><img src=\"/images/tele_icon.png\"/>Questions about pricing?  Main  Contact us at: 1-888-391-3323</span>\n					</div>\n            </nav>\n</div>\n\n\n\n";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.subPage),stack1 == null || stack1 === false ? stack1 : stack1.status), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n";
  return buffer;
  });
})();