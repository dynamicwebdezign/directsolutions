$(function($){
	var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
	
	
	var fancyOptions = {
			maxWidth	: 704,
			maxHeight	: 396,
			fitToView	: false,
			width		: '100%',
			height		: '40%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
			beforeLoad: function(e){
				if(isMobile.any()){
					$.fancybox.close( true );
					window.open('http://animoto.com/play/IYvt3Ouz7gmwmoTbBipYVw','_blank');
				}
			
			}
		};
	

	$(".videopop").click(function(e){
		e.preventDefault();
		console.log($.fancybox);
		$.fancybox.open( {type:'iframe', href:'directsolutionsvideo.html'}, fancyOptions );
	});
	
	
	
	var path = window.location.pathname;
	var page = path.substr( path.lastIndexOf("/") + 1 );
	var html,obj;
	switch(page){
	
	case "whatweoffer.html":
		obj ={ "whatisActive" : true , "subPage": {"status":true , "title": "What We Offer" } };
		break;
	case"about.html":
		obj ={ "aboutisActive" : true, "subPage": {"status":true , "title": "About Us" }  };
		break;
	case "testimonials.html":
		obj ={ "testimonialsisActive" : true , "subPage": {"status":true , "title": "Testimonials" } };
		break;
	case "contact.html":
		obj ={ "contactisActive" : true , "subPage": {"status":true , "title": "Contact Us" } };
		break;
	
		default:
			obj ={ "homeisActive" : true };	
	}
	try{
		
		var template = Handlebars.templates['header.hb'];
		console.log(template);
		//var html = template(obj,{});
		
		$('#header').append(template(obj,{}));
		
//	var compiledTemplate = Handlebars.templates['header.hb'];
	//html = compiledTemplate(obj);
	//$('#header').append(html);
	}catch(e){
		alert(e);
	}

	var compiledTemplate = Handlebars.templates['footer.hb'];
	var html = compiledTemplate();
	$('#mainfooter').append(html);
	
	
	
});	
