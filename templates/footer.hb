	<footer class="footer-area container">
		<div class="row" style="max-width:946px;margin:auto;">
			<section class="col-md-12">
				
				<article class="col-md-3">
					<div id="text-2" class="footer-widget-col widget_text">
						<div class="textwidget">
							<p><img alt="Outsourced Quality Assured Inc." src="images/directcc_footerlogo.png"/></p>
						</div>
					</div>
				</article>

				<article class="col-md-3">
					<div id="recent-posts-3" class="footer-widget-col widget_recent_entries">		
						<h3>Site Map</h3>		
						<ul>
							<li><a href="index.html">Home</a></li>
							<li><a href="whatweoffer.html">What We Offer</a></li>
							<li><a href="about.html">About Us</a></li>
							<li><a href="testimonials.html">Testimonials</a></li>
							<li><a href="contact.html">Contact Us Now</a></li>
						</ul>
					</div>
				</article>

				<article class="col-md-3">
					<div id="text-3" class="footer-widget-col widget_text"><h3>CONTACT US</h3>	
						<div class="textwidget">
							<div>
								<label>TEL:</label> TEL : 1-888-391-3323
							</div>
						</div>
					</div>	
				</article>

				<article class="col-md-3" style="padding-right: 0px;">
					<div id="text-4" class="footer-widget-col widget_text">			
						<div class="textwidget">
							<h3>Watch our video</h3>
							<a class="videopop" data-fancybox-type="iframe"  href="directsolutionsvideo.html">
								<div class="wpvl_auto_thumb_box_wrapper">
									<div class="wpvl_auto_thumb_box" style="width: 198px; height: 110px;">
										<img src="images/video_thumb.jpg" class="video_lightbox_auto_anchor_image" alt="" style="width: 198px; height: 110px;"/>
									</div>
								</div>
							</a>
						</div>
					</div>	
				</article>

			</section>
		</div>
	</footer>
	<footer id="footer" class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="copyright">
					<li>Copyright &copy; 2013 DirectSolutions.cc | All Rights Reserved</li>
				</ul>
				<div class="fr">Site Design By: DynamicWebDezign</div>
			</div>	
		</div>
	</footer>