<div class="avada-row" style="margin-top:0px;margin-bottom:0px;">
            <div class="logo" style="margin-left:0px;">
            <a href="index.html">
           		 <div><img src="images/master_logo_.jpg" alt=""></div>
            </a>
             <span class="vertical-menu-holder">
               <dl class="mobile-vertical-menu">
               	<dd>
                <a href="index.html">Home</a></dd><dd><a href="whatweoffer.html">What We Offer</a></dd><dd><a href="about.html">About Us</a></dd><dd><a href="testimonials.html">Testimonials</a></dd><dd><a href="contact.html">Contact Us Now</a></dd>
                <div class="ac">
					<span class="teleicon"><img src="/images/tele_icon.png"/>Questions about pricing?<br/>    Contact us at: 1-888-391-3323</span>
					</div>
               </dl>
               
               		
               
             </span>
            </div>
              
              
            <nav id="nav" class="nav-holder">
	                <ul id="nav" class="menu">
					<li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page {{#if homeisActive}} current-menu-item page_item page-item-20 current_page_item{{/if}} menu-item-48"><a href="index.html">Home</a></li>
					<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page {{#if whatisActive}}current-menu-item page_item page-item-20 current_page_item{{/if}}menu-item-53"><a href="whatweoffer.html">What We Offer</a></li>
					<li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page {{#if aboutisActive}}current-menu-item page_item page-item-20 current_page_item{{/if}}menu-item-52"><a href="about.html">About Us</a></li>
					<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page {{#if testimonialsisActive}}current-menu-item page_item page-item-20 current_page_item{{/if}}menu-item-51"><a href="testimonials.html">Testimonials</a></li>
					<li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page {{#if contactisActive}}current-menu-item page_item page-item-20 current_page_item{{/if}}menu-item-50"><a href="contact.html">Contact Us Now</a></li>
					</ul>

					<div class="ac contactgotoinfo">
					<span class="teleicon"><img src="/images/tele_icon.png"/>Questions about pricing?  Main  Contact us at: 1-888-391-3323</span>
					</div>
            </nav>
</div>



{{#if subPage.status}}
<div class="page-title-container">
        <div class="page-title">
            <div class="page-title-wrapper">
                <div class="h-left">
                    <div class="h-left-content">
                        <h1>{{subPage.title}}</h1>
                        <ul class="breadcrumbs"><li> <a href="index.html">Home</a></li><li>{{subPage.title}}</li></ul>                                                                    </div>
                </div>
            </div>
        </div>
    </div>
{{/if}}

